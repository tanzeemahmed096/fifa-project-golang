package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/tanzeem/fifastats/err"
	"golang.org/x/exp/constraints"
)

func JsonMarshal(filePath string, matches *[]map[string]interface{}) {
	fileData, fileErr := ioutil.ReadFile(filePath)
	err.CheckError(fileErr)
	unmarshalErr := json.Unmarshal(fileData, &matches)
	err.CheckError(unmarshalErr)
}

func JsonUnmarshal[T constraints.Ordered](filePath string, writeData map[string]T) {
	jsonFile, jsonErr := os.Create(filePath)
	err.CheckError(jsonErr)

	defer jsonFile.Close()

	jsonData, dataErr := json.Marshal(writeData)
	err.CheckError(dataErr)

	_, writeErr := jsonFile.Write(jsonData)
	err.CheckError(writeErr)

	fmt.Println("File write completed")
}
