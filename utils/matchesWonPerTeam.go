package utils

import (
	"path/filepath"
	"strconv"
	"strings"
)

func MatchesWonPerTeam(filePath string) {
	var matches []map[string]interface{}
	JsonMarshal(filePath, &matches)

	teamsMap := make(map[string]float64)

	for _, match := range matches {
		if match["Home Team Goals"].(float64) > match["Away Team Goals"].(float64) {
			teamsMap[match["Home Team Name"].(string)]++
		} else if match["Home Team Goals"].(float64) < match["Away Team Goals"].(float64) {
			teamsMap[match["Away Team Name"].(string)]++
		} else {
			winCondition := match["Win conditions"].(string)
			idx1 := strings.Index(winCondition, "(")
			idx2 := strings.Index(winCondition, ")")

			if idx1 != -1 && idx2 != -1 {
				goalsCount := winCondition[strings.Index(winCondition, "(")+1 : strings.Index(winCondition, ")")]
				parts := strings.Split(goalsCount, " - ")

				homeGoals, _ := strconv.Atoi(parts[0])
				awayGoals, _ := strconv.Atoi(parts[1])

				if homeGoals > awayGoals {
					teamsMap[match["Home Team Name"].(string)]++
				} else if homeGoals < awayGoals {
					teamsMap[match["Away Team Name"].(string)]++
				}
			}
		}
	}

	JsonUnmarshal(filepath.Join(".", "output", "matchesWonPerTeam.json"), teamsMap)
}
