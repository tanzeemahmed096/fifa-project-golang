package utils

import (
	"path/filepath"
	"sort"
	"strings"
)

type Pair struct {
	PlayerName string  `json:"playername"`
	Value      float64 `json:"probabilityOfGoals"`
}

func ProbabilityOfGoalPlayers(filePath string) {
	var players []map[string]interface{}
	JsonMarshal(filePath, &players)

	goalsMap := make(map[string]float64)
	matchesMap := make(map[string]float64)

	for _, player := range players {
		matchesMap[player["Player Name"].(string)]++

		if event, ok := player["Event"].(string); ok {
			if strings.Contains(event, "G") {
				goalsMap[player["Player Name"].(string)] = 1
			}
		}

	}

	probabilityGoalsMap := make(map[string]float64)

	for player, goals := range goalsMap {
		probabilityGoalsMap[player] = goals / matchesMap[player]
	}

	var pairs []Pair
	for key, value := range probabilityGoalsMap {
		pairs = append(pairs, Pair{PlayerName: key, Value: value})
	}

	sort.Slice(pairs, func(i, j int) bool {
		return pairs[i].Value > pairs[j].Value
	})

	pairs = pairs[0:10]

	probabilityOfGoals := make(map[string]float64)

	for _, player := range pairs {
		probabilityOfGoals[player.PlayerName] = player.Value
	}

	JsonUnmarshal(filepath.Join(".", "output", "probabilityOfGoalOfPlayers.json"), probabilityOfGoals)

}
