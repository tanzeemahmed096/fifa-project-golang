package utils

import (
	"path/filepath"
	"strings"
)

type Match struct {
	HomeTeamName    string
	AwayTeamName    string
	HomeTeamInitial string
	AwayTeamInitial string
	Year            float64
}

func NumberOfRedCardsPerTeam(worldCupMatches, worldCupPlayers string, year int) {
	var matches []map[string]interface{}
	JsonMarshal(worldCupMatches, &matches)

	matchData := make(map[float64]Match)

	for _, match := range matches {
		matchData[match["MatchID"].(float64)] = Match{HomeTeamName: match["Home Team Name"].(string), AwayTeamName: match["Away Team Name"].(string), HomeTeamInitial: match["Home Team Initials"].(string), AwayTeamInitial: match["Away Team Initials"].(string), Year: match["Year"].(float64)}
	}

	var players []map[string]interface{}
	JsonMarshal(worldCupPlayers, &players)

	redCardsTeam := make(map[string]int)

	for _, player := range players {
		redCardPlayer := false
		if event, ok := player["Event"].(string); ok {
			redCardPlayer = strings.Contains(event, "R")
		}

		if redCardPlayer {
			var matchDetail Match = matchData[player["MatchID"].(float64)]
			teamInitial := player["Team Initials"].(string)

			if teamInitial == matchDetail.HomeTeamInitial && float64(year) == matchDetail.Year {
				redCardsTeam[matchDetail.HomeTeamName]++
			} else if teamInitial == matchDetail.AwayTeamInitial && float64(year) == matchDetail.Year {
				redCardsTeam[matchDetail.AwayTeamInitial]++
			}
		}
	}

	JsonUnmarshal(filepath.Join(".", "output", "numberOfRedCardsPerTeam.json"), redCardsTeam)
}
