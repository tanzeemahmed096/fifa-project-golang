package utils

import "path/filepath"

func MatchesPerCityCount(filePath string) {

	var matches []map[string]interface{}
	JsonMarshal(filePath, &matches)

	matchesPerCity := make(map[string]int)

	for _, match := range matches {
		if city, ok := match["City"].(string); ok {
			matchesPerCity[city]++
		}
	}

	JsonUnmarshal(filepath.Join(".", "output", "matchesPerCity.json"), matchesPerCity)
}
