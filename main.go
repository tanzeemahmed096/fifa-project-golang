package main

import (
	"path/filepath"

	"github.com/tanzeem/fifastats/utils"
)

func main() {
	dir := "data"

	// 1. Matches per city
	fullPathMatches := filepath.Join(".", dir, "WorldCupMatches.json")
	utils.MatchesPerCityCount(fullPathMatches)

	//2. Matches won per team
	utils.MatchesWonPerTeam(fullPathMatches)

	//3. Number of Red Cards Per team
	fullPathPlayers := filepath.Join(".", dir, "WorldCupPlayers.json")
	utils.NumberOfRedCardsPerTeam(fullPathMatches, fullPathPlayers, 2014)

	//4. probability of highest goal scoring players
	utils.ProbabilityOfGoalPlayers(fullPathPlayers)
}
